var Pencil = require('./tools/pencil.js');
var Brush = require('./tools/brush.js');

module.exports = {
	Pencil: Pencil,
	Brush: Brush
};