function PubSub() {
	this.handlers = {};
}

PubSub.prototype.subscribe = function(event, handler, context) {
	if (typeof context === 'undefined') {
		context = handler;
	}

	if (!this.handlers[event]) {
		this.handlers[event] = [];
	}

	this.handlers[event].push(handler.bind(context));
};

PubSub.prototype.publish = function(event, args) {
	if (!this.handlers[event]) return;

	this.handlers[event].forEach(function(handler) {
		handler(args);
	});
};

module.exports = PubSub;