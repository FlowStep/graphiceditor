module.exports = (function() {
	var history = [];
	var undoHistory = [];

	function appendElement(element) {
		history.push(element);

		if (undoHistory.length != 0) {
			undoHistory = [];
		}
	}

	function undo() {
		if (history.length == 0) return;

		undoHistory.push(history.pop());
	}

	function redo() {
		if (undoHistory.length == 0) return;

		history.push(undoHistory.pop());
	}

	return {
		history: history,
		appendElement: appendElement,
		undo: undo,
		redo: redo 
	};
})();