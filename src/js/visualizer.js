var PubSub = require('./pubsub.js');

var visualizer = (function() {
	var canvasContainer = document.querySelector('.editor');

	var canvas = document.createElement('canvas');
	canvas.setAttribute('width', canvasContainer.offsetWidth);
	canvas.setAttribute('height', canvasContainer.offsetHeight);
	var ctx = canvas.getContext('2d');

	canvasContainer.appendChild(canvas);

	var pubsub = new PubSub();

	function getCanvasCoords(event) {
		return {
			canvasX: event.clientX - canvas.getBoundingClientRect().left,
			canvasY: event.clientY - canvas.getBoundingClientRect().top
		}
	}

	var isDrawing = false;

	canvas.addEventListener('mousedown', function(e) {
		isDrawing = true;

		var eventObj = {};

		pubsub.publish('startdrawing', getCanvasCoords(e));
	});

	canvas.addEventListener('mousemove', function(e) {
		if (!isDrawing) {
			return;
		}

		pubsub.publish('continuedrawing', getCanvasCoords(e));
	});

	canvas.addEventListener('mouseup', function(e) {
		if (!isDrawing) {
			return;
		}

		isDrawing = false;

		pubsub.publish('stopdrawing', getCanvasCoords(e));
	});

	canvas.addEventListener('mouseout', function(e) {
		if (!isDrawing) {
			return;
		}
		
		isDrawing = false;

		pubsub.publish('interruptdrawing', getCanvasCoords(e));
	});

	document.addEventListener('keydown', function(e) {
		//TODO: Проверить есть ли фокус на элементе приложения

		if (e.keyCode != 90) {
			return;
		}

		if (!e.ctrlKey && !e.metaKey) {
			return
		}

		if (e.shiftKey) {
			pubsub.publish('redo');
			return;
		}

		pubsub.publish('undo');
	});

	var on = function(event, handler, context) {
		pubsub.subscribe(event, handler, context);
	};

	var clear = function() {
		ctx.clearRect(0, 0, canvas.width, canvas.height);
	}

	var drawElementFunctions = {
		brush: function(line) {
			ctx.beginPath();
			ctx.moveTo(line.points[0].x, line.points[0].y);

			for (var i = 1; i < line.points.length; ++i) {
				ctx.lineTo(line.points[i].x, line.points[i].y);
			}

			ctx.stroke();
		}
	}

	var drawElement = function(element) {
		drawElementFunctions[element.type](element);
	};

	var drawModel = function(model) {
		for (var i = 0; i < model.history.length; ++i) {
			drawElement(model.history[i]);
		}
	};

	var redrawModel = function(model) {
		clear();
		drawModel(model);
	};

	return {
		drawingContext: ctx,
		on: on,
		clear: clear,
		drawElement: drawElement,
		drawModel: drawModel,
		redrawModel: redrawModel
	};
})();

module.exports = visualizer;