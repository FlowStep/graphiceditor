var Immediate = require('./immediate');

function Brush() {
	Immediate.apply(this, arguments);

	this.type = 'brush';
}

Brush.prototype = Object.create(Immediate.prototype);
Brush.prototype.constructor = Brush;

Brush.prototype.startDrawing = function(x, y) {
	Immediate.prototype.startDrawing.apply(this, arguments);
};

Brush.prototype.continueDrawing = function(x, y) {
	Immediate.prototype.continueDrawing.apply(this, arguments);

	return this._points;
};

Brush.prototype.stopDrawing = function(x, y) {
	Immediate.prototype.stopDrawing.apply(this, arguments);

	return this._points;
};

module.exports = Brush;