function Tool() {
	this._drawingContext = null;
}

Tool.prototype.bindContext = function(drawingContext) {
	this._drawingContext = drawingContext;
}

Tool.prototype.startDrawing = function(x, y) {
	console.group();
	console.log(this.type + ': startDrawing');

	if (!this._drawingContext) {
		throw Error('No drawing context');
	}
};

Tool.prototype.continueDrawing = function(x, y) {
	console.log(this.type + ': continueDrawing');

	if (!this._drawingContext) {
		throw Error('No drawing context');
	}
};

Tool.prototype.stopDrawing = function(x, y) {
	console.log(this.type + ': stopDrawing');
	console.groupEnd();

	if (!this._drawingContext) {
		throw Error('No drawing context');
	}
};

module.exports = Tool;