var Tool = require('./tool.js');

function Immediate() {
	Tool.apply(this, arguments);

	this._points = [];
}

Immediate.prototype = Object.create(Tool.prototype);
Immediate.prototype.constructor = Immediate;

Immediate.prototype.startDrawing = function(x, y) {
	Tool.prototype.startDrawing.apply(this, arguments);

	this._points = [];
	this._points.push({x: x, y: y});
};

Immediate.prototype.continueDrawing = function(x, y) {
	Tool.prototype.continueDrawing.apply(this, arguments);

	this._points.push({x: x, y: y});
};

Immediate.prototype.stopDrawing = function(x, y) {
	Tool.prototype.stopDrawing.apply(this, arguments);
};

module.exports = Immediate;