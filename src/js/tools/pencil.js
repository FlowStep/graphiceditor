var Immediate = require('./immediate');

function Pencil() {
	Immediate.apply(this, arguments);

	this.type = 'pencil';
}

Pencil.prototype = Object.create(Immediate.prototype);
Pencil.prototype.constructor = Pencil;

Pencil.prototype.startDrawing = function(x, y) {
	Immediate.prototype.startDrawing.apply(this, arguments);

	this._drawingContext.moveTo(x, y);
};

Pencil.prototype.continueDrawing = function(x, y) {
	Immediate.prototype.continueDrawing.apply(this, arguments);

	this._drawingContext.lineTo(x, y);
	this._drawingContext.stroke();
};

Pencil.prototype.stopDrawing = function(x, y) {
	Immediate.prototype.stopDrawing.apply(this, arguments);
};

module.exports = Pencil;