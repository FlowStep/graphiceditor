var Tools = require('./tools.js');
var visualizer = require('./visualizer.js');
var model = require('./model.js');

var controller = (function() {
	visualizer.drawingContext.fillStyle = "rgba(0,0,0,1)";
	visualizer.drawingContext.strokeStyle = "rgba(0,0,0,0.5)";
	visualizer.drawingContext.lineWidth = 10;
	visualizer.drawingContext.lineCap = 'round';
	visualizer.drawingContext.lineJoin = 'round';

	console.dir(model);

	var tool = new Tools.Brush();
	tool.bindContext(visualizer.drawingContext);

	visualizer.on('startdrawing', function(e) {
		tool.startDrawing(e.canvasX, e.canvasY);
	});

	visualizer.on('continuedrawing', function(e) {
		var points = tool.continueDrawing(e.canvasX, e.canvasY);
		visualizer.redrawModel(model);
		visualizer.drawElement({
			type: tool.type,
			points: points
		});
	});

	visualizer.on('stopdrawing', function(e) {
		var points = tool.stopDrawing(e.canvasX, e.canvasY);
		model.appendElement({
			type: tool.type,
			points: points
		});
	});

	visualizer.on('interruptdrawing', function(e) {
		var points = tool.stopDrawing(e.canvasX, e.canvasY);
		model.appendElement({
			type: tool.type,
			points: points
		});
	});

	visualizer.on('redo', function() {
		model.redo();
		visualizer.redrawModel(model);
	});

	visualizer.on('undo', function () {
		model.undo();
		visualizer.redrawModel(model);		
	});
})();

module.exports = controller;